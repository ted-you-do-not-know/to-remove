import asyncio
import argparse
from sitetrackers import main as track
from concurrent.futures import ProcessPoolExecutor
import csv
import json
from traceback import format_exc


async def task(n, domain_data_rows):
    a = {}
    try:
        for item in domain_data_rows:
            idx = item['idx']
            domain = item['profile']
            result = await track(domain)
            if result:
                a[idx] = 1
    except Exception as e:
        print(format_exc())
    finally:
        with open(f'../results/result_{n}.json', 'w') as h:
            json.dump(a, h)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="haha")
    parser.add_argument('--worker')
    worker = parser.parse_args().worker

    with open(f'../profiles/profile_{worker}.csv', 'r') as f:
        rows = []
        for r in csv.DictReader(f):
            rows.append(
                {
                    'idx': r['idx'],
                    'profile': r['profile']
                }
            )

    leng = len(rows)
    print(len(rows))
    loop = asyncio.get_event_loop()
    loop.run_until_complete(task(str(worker), rows))
