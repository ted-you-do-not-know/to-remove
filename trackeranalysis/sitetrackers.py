from trackerutil import TrackerUtil
import asyncio
from pyppeteer import launch
from urllib.parse import urlparse

util = TrackerUtil()

async def analyze_tracker(req, all_trackers):
    print('.', end = '', flush = True)
    trackers = util.get_by_url(req.url)
    if trackers:
        for t in trackers:
            if t['name'] not in all_trackers:
                all_trackers[t['name']] = t
                print('*', end = '', flush = True)
    await req.continue_()


async def main(site):
    trackers = {}
    if not site.startswith('http'):
        site = 'https://' + site
    print('Analyzing', site)

    try:
        browser = await launch()
        page = await browser.newPage()
        await page.setRequestInterception(True)
        page.on('request', lambda req: analyze_tracker(req, trackers))
        await page.goto(site, waitUntil = 'networkidle0')
        await browser.close()
        if trackers:
            print(trackers)
            return True
        return False
    except KeyboardInterrupt as ex:
        return False
    except Exception as ex:
        return False


async def go(site):
    asyncio.get_event_loop().run_until_complete(main(site))
#
#
# if __name__ == '__main__':
#     site = 'suprint.jp'
#     asyncio.get_event_loop().run_until_complete(main(site))
