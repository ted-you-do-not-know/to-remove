import csv

# FILE = '../all-jp-pte.csv'
# out_handler = open('../all.csv', 'w')
# writer = csv.DictWriter(out_handler, fieldnames=['idx', 'account_id', 'email', 'profiles', 'last_active_time', 'karte.io'])
# writer.writeheader()
#
# with open(FILE, 'r') as r:
#     reader = csv.DictReader(r)
#     las = []
#     i = 1
#     for idx, row in enumerate(reader):
#         print(i)
#         account_id = row['account_id']
#         profiles = row['profile_domain_list']
#         last_active_time = row['last_active_time']
#         email = row['email']
#         if profiles:
#             if last_active_time:
#                 writer.writerow({
#                     'idx': i,
#                     'account_id' : account_id,
#                     'email': email,
#                     'profiles': profiles,
#                     'last_active_time': last_active_time,
#                     'karte.io': ''
#                 })
#                 i += 1
#             else:
#                 las.append(row)
#     for row in las:
#         print(i)
#         account_id = row['account_id']
#         profiles = row['profile_domain_list']
#         last_active_time = row['last_active_time']
#         email = row['email']
#         writer.writerow({
#             'idx': i,
#             'account_id': account_id,
#             'email': email,
#             'profiles': profiles,
#             'last_active_time': last_active_time,
#             'karte.io': ''
#         })
#         i += 1

all = []
FILE = '../all.csv'
# out_handler = open('../profiles.csv', 'w')
# writer = csv.DictWriter(out_handler, fieldnames=['idx', 'profile'])
# writer.writeheader()


def get_writer(n):
    out_handler = open(f'../profiles/profile_{n}.csv', 'w')
    writer = csv.DictWriter(out_handler, fieldnames=['idx', 'profile'])
    writer.writeheader()
    return writer, out_handler


def clean_domain(domain):
    if not domain:
        return domain
    res = domain.strip().strip('/')
    return res


with open('../all.csv', 'r') as f:
    for i, row in enumerate(csv.DictReader(f)):
        print(i)
        profiles = row['profiles']
        if profiles:
            if ',' in profiles:
                domains = profiles.split(',')
            else:
                domains = profiles.split(';')
            all.extend(domains)

all_ = set([clean_domain(d) for d in all if clean_domain(d)])

limit = 4000
count = 0
worker = 0
writer, handler = get_writer(worker)
try:
    for idx, a in enumerate(all_):
        if count > 4000:
            count = 0
            worker += 1
            handler.close()
            writer, handler = get_writer(worker)
        writer.writerow(
            {'idx': idx,
             'profile': a}
        )
        count += 1
finally:
    handler.close()

