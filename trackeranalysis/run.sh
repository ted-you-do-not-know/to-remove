#!/usr/bin/env bash

ps -ef | grep pyppeteer | cut -c 9-15| xargs kill -s 9
ps -ef | grep go_go | cut -c 9-15| xargs kill -s 9

for((i=0;i<=21;i++));
do
 nohup /home/ted/datadeck-python/leadsgrading-single-dev/venv/bin/python3.7 go_go.py --worker $i >> ../logs/tracker-$i.log &
done
