import json
from collections import defaultdict
from urllib.parse import urlparse

class TrackerUtil:

    def __init_host_path_map(self, bugs, apps):
        self.tracker_info_with_host_path = []
        def dfs(d, curr_path):
            if not d:
                return
            for k, v in d.items():
                if k == '$':
                    for e in v:
                        p = curr_path + '/' + e['path']
                        yield (p, e['id'])
                else:                      
                    p = k
                    if curr_path:
                        p += '.' + curr_path
                    for item in dfs(v, p):
                        yield item

        for path, bug_id in dfs(self.tracker_db['patterns']['host_path'], ''):
            aid = bugs[str(bug_id)]['aid']
            app = apps[str(aid)]
            r = { 'name': app['name'], 
                'id': int(aid), 
                'cat': app['cat'],
                'host_path': path }
            self.tracker_info_with_host_path.append(r)

    def __init_domain_map(self, bugs, apps):
        self.tracker_info_with_domains = {}

        # get domains from 'firstPartyExceptions' map
        exps = self.tracker_db['firstPartyExceptions'] # bug_id => domains
        bugs_mapping = defaultdict(set)
        for (k, v) in bugs.items():
            if k in exps:
                exp = exps[k]
                aid = v['aid']
                domains = bugs_mapping[aid].union(exp)
                bugs_mapping[aid] = set(domains)

        for key, app in apps.items():
            r = { 'name': app['name'], 'id': int(key), 'cat': app['cat'] }
            self.tracker_info_with_domains[app['name'].lower()] = r
            if int(key) in bugs_mapping:
                domains = bugs_mapping[int(key)]
                r['domains'] = domains
            else:
                r['domains'] = set()

        # get domains from 'host' map
        def dfs(d, curr_path):
            if not d:
                return
            for k, v in d.items():
                if k == '$':
                    yield (curr_path, v)
                else:                      
                    p = k
                    if curr_path:
                        p += '.' + curr_path
                    for item in dfs(v, p):
                        yield item

        for domain, bug_id in dfs(self.tracker_db['patterns']['host'], ''):
            aid = bugs[str(bug_id)]['aid']
            app = apps[str(aid)]
            name = app['name'].lower()
            if name in self.tracker_info_with_domains and 'domains' in self.tracker_info_with_domains[name]:
                self.tracker_info_with_domains[name]['domains'].add(domain)
            else:
                self.tracker_info_with_domains[name]['domains'] = set([domain])

        # normalize domain set to list for easier dumping
        for v in self.tracker_info_with_domains.values():
            v['domains'] = list(v['domains'])

    def __init__(self):
        with open('trackerdb.json', 'rt', encoding = 'utf-8') as f:
            self.tracker_db = json.load(f)
            self.domain_lookup_cache = {}
            self.host_path_lookup_cache = {}

            apps = self.tracker_db['apps']
            bugs = self.tracker_db['bugs'] # 'bug_id' => aid
 
            self.__init_domain_map(bugs, apps)
            self.__init_host_path_map(bugs, apps)
            # todo

            self.tracker_info_with_host_path = []
            self.tracker_info_with_domains = {}
            self.tracker_info_with_host_path.append(
                {
                    "name": "karte",
                    "id": 9999,
                    "cat": "custom",
                    "host_path": "static.karte.io/libs/tracker.js"
                }
            )

            self.tracker_info_with_domains['karte io'] = {
                'name': 'Karte io',
                'id': 9999,
                'cat': 'site_analytics',
                'domains': ['suprint.jp']
            }

    def get_all_info(self):
        r = { 'domain': self.tracker_info_with_domains, 'host_path': self.tracker_info_with_host_path }
        return r

    def get_info(self, tracker_name):
        name = tracker_name.lower()
        if name in self.tracker_info_with_domains:
            return self.tracker_info_with_domains[name]
        for item in self.tracker_info_with_host_path:
            if item['name'] == name:
                return item

    def get_by_domain(self, domain):
        '''
        Get tracker info by domain name, e.g., stats.g.doubleclick.net
        '''

        if domain in self.domain_lookup_cache:
            return self.domain_lookup_cache[domain]

        result = []
        for v in self.tracker_info_with_domains.values():
            if 'domains' in v:
                for d in v['domains']:
                    if d in domain.lower():
                        result.append(v)
                        break
        self.domain_lookup_cache[domain] = result
        return result

    def get_by_host_path(self, host_path):
        '''
        Get tracker info by host path, e.g., www.google-analytics.com/collect
        '''
        
        if host_path in self.host_path_lookup_cache:
            return self.host_path_lookup_cache[host_path]

        r = []
        for item in self.tracker_info_with_host_path:
            if item['host_path'] in host_path:
                r = [item]
                break
        self.host_path_lookup_cache[host_path] = r
        return r

    def get_by_url(self, url):
        '''
        Get tracker info by url, e.g., https://stats.g.doubleclick.net/dc.js.
        This method tries with domain name first, then host path
        '''

        parsed = urlparse(url)
        r = self.get_by_domain(parsed.netloc)
        if r:
            return r
        path = parsed.netloc + parsed.path
        r = self.get_by_host_path(path)
        return r


if __name__ == '__main__':
    util = TrackerUtil()
    print(util.get_info('Tealium'))
    with open('tracker_db_parsed.json', 'wt', encoding = 'utf-8') as fo:
        json.dump(util.get_all_info(), fo, sort_keys = True, indent = 4)

    domain = 'stats.g.doubleclick.net'
    info = util.get_by_domain(domain)
    print(info)

    host_path = 'www.google-analytics.com/collect'
    info = util.get_by_host_path(host_path)
    print(info)

    info = util.get_by_url('https://www.google-analytics.com/collect?v=1&_v=j68&a=1863840903&t=pageview&_s=1&dl=https%3A%2F%2Fwww.ptengine.jp%2F&ul=zh-cn&de=UTF-8&dt=%E3%83%92%E3%83%BC%E3%83%88%E3%83%9E%E3%83%83%E3%83%97%E4%BB%98%E3%81%8D%E3%82%A2%E3%82%AF%E3%82%BB%E3%82%B9%E8%A7%A3%E6%9E%90%E3%83%84%E3%83%BC%E3%83%AB%20%7C%20Ptengine&sd=24-bit&sr=1920x1080&vp=1425x759&je=0&_utma=104755137.545876022.1508757638.1531399258.1531471653.52&_utmz=104755137.1509064188.1.1.utmcsr%3D(direct)%7Cutmccn%3D(direct)%7Cutmcmd%3D(none)&_utmht=1531473714242&_u=yCCCAEADQ~&jid=&gjid=&cid=545876022.1508757638&uid=545876022.1508757638&tid=UA-112701314-1&_gid=1011651031.1531302802&gtm=G6t5G6G3QJ&cd4=&cd5=&cd6=&cd7=&cd18=545876022.1508757638&z=45610899')
    print(info)

    info = util.get_by_url('https://stats.g.doubleclick.net/dc.js')
    print(info)

    info = util.get_by_url('https://po.st/dc.js')
    print(info)
